# kisters.network_store.model_library.water

The models for the `water` domain of the kisters.network_store.model_library

The library is quite extensive, though cannot cover all the possible applications.

In case of the lack of predefined properties, please use the 'user_metadata' field
in order to maintain compatibility with other network_store services!
Please note that only 'str' keys and (str|bool|int|float) values are accepted there!
